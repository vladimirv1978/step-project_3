
import {fetchGetAllCards} from './api.js'
import createModalVisit from "./chooseDoctor.js"
import {createdCardDoctor} from './visit.js'

function getCards() {
    let logInBtn = document.querySelector(".header__button-login");
    // logInBtn.removeEventListener("click", openModalWindow);
    logInBtn.innerText = "Create Visit"
    logInBtn.addEventListener("click", () => {
      new createModalVisit().render();
    });
    fetchGetAllCards().then((cards) => {
      if (cards.length !== 0){
        console.log(cards);
        // document.querySelector(".body__empty-page").remove()
        cards.forEach((card) => {createdCardDoctor(card)})
      } else {
        console.log('the visits is empty')
        // document.querySelector(".card_container").innerHTML = `<h2 class="empty-card">the visits is empty</h2>`
      }
    })
  };
    
    // if (JSON.parse(getCards).length === 0) {
    //   document.querySelector(".header__button-login").innerText = "Create Visit";
      // document.querySelector(
      //   ".card_container"
      // ).innerHTML = `<h2 class="empty-card">the visits is empty</h2>`;
    // } else {
    //   document.querySelector(".header__button-login").innerText = "Create Visit";
    //   // переписать на вызов модалки ивент листенер обязателен
    //   JSON.parse(getCards).forEach((element) => {
    //     console.log(element);
    //     // const { doctor } = element;
    //     // checkCard(doctor, element);
    //   });
    // }

    export default getCards;