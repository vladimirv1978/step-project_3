import Modal from "./modal.js";

const openModalWindow = () => {
    new Modal().render();
  };
  

  export default openModalWindow;