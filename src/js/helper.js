export default function creatElement(tagName, className, contentText = '') {
    let element = document.createElement(tagName);
    element.className = className;
    element.innerText = contentText;
    return element;
  }