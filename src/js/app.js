import * as flsFunctions from "./modules/functions.js";
flsFunctions.isWebp(); //не убирать, обязательно для функционирования

import * as classVisit from  "./visit.js"

import openModalWindow from "./openModal.js";
import getCards from "./getCards.js";
import getSectionFilter from "./filter.js";

const logInBtn = document.querySelector(".header__button-login");

if (sessionStorage.getItem("tokenData")) {
    let header = document.querySelector(".header__logo");
    getSectionFilter(header);
    getCards();
} else {
    logInBtn.addEventListener("click", openModalWindow);
}