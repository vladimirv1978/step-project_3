import creatElement from "./helper.js";
import { fetchGetAllCards } from "./api.js";
import { createdCardDoctor } from "./visit.js";

const placeWithCards = document.querySelector(".card__fild");

function getAllCards(newArrayCards) {
    if (newArrayCards.length > 0) {
        newArrayCards.forEach((card) => {
          createdCardDoctor(card);
        });
      } else {
        placeWithCards.innerText = "Карток не знайдено";
      }
}

//ф-ція створення випадаючого списку для сортування
function createDropdown(parentElement, dropdownName, arrayOptions = []) {
  let select = creatElement("select", `select-${dropdownName}`);
  select.id = dropdownName;
  let label = creatElement(
    "label",
    "",
    dropdownName.charAt(0).toUpperCase() + dropdownName.slice(1) + ":"
  );
  label.setAttribute("for", dropdownName);
  let optionTitle = creatElement("option", "", `Select ${dropdownName}:`);
  select.append(optionTitle);
  select.addEventListener("change", (e) => {
    let isActive = e.target.value;
    if (isActive != `Select ${dropdownName}:`) {
      optionTitle.style.display = "none";
    }
  });
  arrayOptions.forEach((element) => {
    let option = creatElement("option", "", element);
    option.setAttribute("value", element);
    select.append(option);
  });
  parentElement.append(label, select);
}

//ф-ція сортування за випадаючими списками
function filterForStatusAndUrgency() {
  let changeUrgency = document.querySelector(".select-urgency");
  let changeStatus = document.querySelector(".select-status");
  let urgencyValue = changeUrgency.value;
  let statusValue = changeStatus.value;
  if (urgencyValue != "Select urgency:" || statusValue != "Select status:") {
    placeWithCards.innerHTML = "";
    fetchGetAllCards()
      .then((arrayCards) => {
        let newArrayCards = [];
        arrayCards.forEach((card) => {
          if ((card.urgency === urgencyValue) & (card.status === statusValue)) {
            newArrayCards.push(card);
          } else if (
            card.urgency === urgencyValue ||
            card.status === statusValue
          ) {
            newArrayCards.push(card);
          }
        });
        getAllCards(newArrayCards);
      })
      .catch(function (error) {
        console.log(error);
      });
  }
}

//ф-ція створення кнопки пошуку та сортування
function createFilterBtn(parentElement) {
  let btn = creatElement("button", "btn-search", "Search");
  btn.addEventListener("click", filterForInput);
  parentElement.append(btn);
}

function createSortBtn(parentElement) {
    let btn = creatElement("button", "btn-sort", "Show");
    btn.addEventListener("click", filterForStatusAndUrgency);
    parentElement.append(btn);
  }

//ф-ція-фільтр пошуку за інпутом
function filterForInput() {
  let inputSearch = document.querySelector(".input-search");
  let valueSearch = inputSearch.value.toLowerCase();
  if (valueSearch.length > 0) {
    placeWithCards.innerHTML = "";
    fetchGetAllCards()
      .then((arrayCards) => {
        let newArrayCards = [];
        arrayCards.forEach((card) => {
          let desc = card.description.toLowerCase();
          if (desc.indexOf(valueSearch) > -1) {
            newArrayCards.push(card);
          }
        });
        getAllCards(newArrayCards);
      })
      .catch(function (error) {
        console.log(error);
      });
  }
}

//ф-ція створення інпута пошуку
function createFilterInput(parentElement) {
  let search = creatElement("input", "input-search");
  search.setAttribute("placeholder", "Search...");
  parentElement.append(search);
}

//ф-ція - створення секції з фільтром
function getSectionFilter(parentElement) {
  let sectionFilter = creatElement("div", "section-filters");
  createFilterInput(sectionFilter);
  createFilterBtn(sectionFilter);
  createDropdown(sectionFilter, "urgency", ["Low", "Normal", "High"]);
  createDropdown(sectionFilter, "status", ["Open", "Done"]);
  createSortBtn(sectionFilter);
  parentElement.insertAdjacentElement("afterend", sectionFilter);
}

export default getSectionFilter;
