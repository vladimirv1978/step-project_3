import {fetchDeleteCard} from './api.js'

let additionalInfoCard = document.querySelector(".card__fild");

class Visit {
  constructor(doctor, purpose, description, urgency, fullName, status, id) {
    this.doctor = doctor;
    this.purpose = purpose;
    this.description = description;
    this.urgency = urgency;
    this.fullName = fullName;
    this.status = status;
    this.id = id;
  }

  render() {
    let cardFild = document.querySelector(".card__fild");
    let cardWrapper = document.createElement("div");
    cardWrapper.className = "card-wrapper";
    cardWrapper.innerHTML = `<span class="card__patient-name">${this.fullName}</span> <br>
        <span class="card__doctor">${this.doctor}</span> <br>
        <div class="card__button-wrapper">
        <button type="button" class="button card__button-show-more">Показать больше</button>
        <button type="button" class="button card__button-delete">Удалить</button>
        </div>`;
    cardFild.prepend(cardWrapper);

    let buttonShowMore = document.querySelector(".card__button-show-more");
    this.addListnerButtonShowMore(buttonShowMore, cardWrapper);

    let cardButtonDelete = document.querySelector(".card__button-delete");
    this.addListnerButtonDelete(cardButtonDelete, cardWrapper);
  }

  addListnerButtonShowMore(buttonShowMore, cardWrapper) {

    buttonShowMore.addEventListener("click", (e) => {
         this.renderFullCard();
    });
  }

  renderFullCard() {
    let additionalInfoCardWrapper = document.createElement("div");
    additionalInfoCardWrapper.className = "card__additional-info-backgraund";
    additionalInfoCardWrapper.innerHTML = `<div class="card__additional-info-card">
        <h3 class="card__patient-name">Full name: ${this.fullName}</h3> <br>
        <h3 class="card__doctor">Doctor: ${this.doctor}</h3> <br>
        <p class="card__data-item purpose">Purpose: ${this.purpose}</p> <br>
        <p class="card__data-item description">Description of the problem: ${this.description}</p> <br>
        <p class="card__data-item urgency">Urgency: ${this.urgency}</p>
        <button type="button" class="button card__button-edit">Редактировать</button>
        </div>`;
        additionalInfoCard.prepend(additionalInfoCardWrapper);
        additionalInfoCardWrapper.addEventListener('click', (e)=>{
            if(e.target == additionalInfoCardWrapper)additionalInfoCardWrapper.remove()
        })

        const editButton = document.querySelector(".card__button-edit");
        this.addListnerButtonEdit(editButton);
        
  }

  addListnerButtonDelete(cardButtonEdit) {
    cardButtonEdit.addEventListener("click", (event) => {
      const deleteCard = event.currentTarget.closest('.card-wrapper');
      fetchDeleteCard(this)
      .then(
      deleteCard.remove()
      )
    });
  }

  addListnerButtonEdit(editButton){
    editButton.addEventListener("click", (event)=>{
      // const card = document.querySelector(".card__additional-info-card");
      const button = document.querySelector(".button");
      const editButton = document.querySelector(".card__button-edit");
      const editCard = event.currentTarget.closest('.card__additional-info-card');
      let nameValue = editCard.querySelector('.card__patient-name').innerText;
      let fullName = editCard.querySelector('.card__patient-name');
      fullName.innerHTML = `<input type="text" value="${nameValue}"/>`;

      let doctorName = editCard.querySelector(".card__doctor");
      let doctorNameValue = editCard.querySelector(".card__doctor").innerText;
      doctorName.innerHTML = `<input type="text" value="${doctorNameValue}"/>`;
      

      let purposeCard = editCard.querySelector(".purpose");
      let purposeCardValue = editCard.querySelector(".purpose").innerText;
      purposeCard.innerHTML = `<input type="text" value="${purposeCardValue}"/>`;


      let descriptionCard = editCard.querySelector(".description");
      let descriptionValue = editCard.querySelector(".description").innerText;
      descriptionCard.innerHTML = `<input type="text" value="${descriptionValue}"/>`;


      let urgencyCard = editCard.querySelector(".urgency");
      let urgencyCardValue = editCard.querySelector(".urgency").innerText;
      urgencyCard.innerHTML = `<input type="text" value="${urgencyCardValue}"/>`;

      
      button.innerText = "Сохранить";

      // editButton.remove(".card__button-edit")
      // button.innerHTML = `<button type="button" class="button card__button-save">Созранить</button>
      // </div>`;

//треба зробити щось з кнопкою щоб можна було навісити слушатель.



      // по ідеї ці два рядки повинні записати зміни. їх треба запхнути в слушатєль на кнопку сохранить
      // editCard.append(fullName, doctorName, purposeCard, descriptionCard, urgencyCard);
      //   return editCard;

      console.log(nameValue);
      console.log(fullName);
      // card__additional-info-card
    })
    
  }
}

class VisitDentist extends Visit {
  constructor(
    doctor,
    purpose,
    description,
    urgency,
    fullName,
    status,
    id,
    dateLastVisit
  ) {
    super(doctor, purpose, description, urgency, fullName, status, id);
    this.dateLastVisit = dateLastVisit;
  }
  
  renderFullCard() {
      super.renderFullCard();
      let additionalInfoCard = document.querySelector(".card__additional-info-card");
      additionalInfoCard.insertAdjacentHTML('beforeend', `<p class="card__data-item">Последняя дата визита: ${this.dateLastVisit}</p>`)
  }
}

class VisitCardiologist extends Visit {
  constructor(
    doctor,
    purpose,
    description,
    urgency,
    fullName,
    status,
    id,
    normalPressure,
    bodyMassIndex,
    pastDiseasesCS,
    age
  ) {
    super(doctor, purpose, description, urgency, fullName, status, id);
    this.normalPressure = normalPressure;
    this.bodyMassIndex = bodyMassIndex;
    this.pastDiseasesCS = pastDiseasesCS;
    this.age = age;
  }

  renderFullCard() {
    super.renderFullCard();
    let additionalInfoCard = document.querySelector(".card__additional-info-card");
    additionalInfoCard.insertAdjacentHTML('beforeend', 
    `<p class="card__data-item">Normal pressure: ${this.normalPressure}</p>
    <p class="card__data-item">Body mass index: ${this.bodyMassIndex}</p>
    <p class="card__data-item">Past diseases of the cardiovascular system: ${this.pastDiseasesCS}</p>
    <p class="card__data-item">Age: ${this.age}</p>`
    )
}
}

class VisitTherapist extends Visit {
  constructor(doctor, purpose, description, urgency, fullName, status, id, age) {
    super(doctor, purpose, description, urgency, fullName, status, id);
    this.age = age;
  }
}



export function createdCardDoctor(cardPatient) {
    switch (cardPatient.doctor) {
      case "Dentist":
        let dentist = new VisitDentist(
          cardPatient.doctor,
          cardPatient.purpose,
          cardPatient.description,
          cardPatient.urgency,
          cardPatient.fullName,
          cardPatient.status,
          cardPatient.id,
          cardPatient.age
        );
        dentist.render();
        break;
      case "Cardiologist":
        let cardiologist = new VisitCardiologist(
          cardPatient.doctor,
          cardPatient.purpose,
          cardPatient.description,
          cardPatient.urgency,
          cardPatient.fullName,
          cardPatient.status,
          cardPatient.id,
          cardPatient.normalPressure,
          cardPatient.bodyMassIndex,
          cardPatient.pastDiseasesCS,
          cardPatient.age
        );
        cardiologist.render();
        break;
      case "Therapist":
        let therapist = new VisitTherapist(
          cardPatient.doctor,
          cardPatient.purpose,
          cardPatient.description,
          cardPatient.urgency,
          cardPatient.fullName,
          cardPatient.status,
          cardPatient.id,
          cardPatient.age
        );
        therapist.render();
        break;
      default:
        console.log("No medical records created");
    }
}