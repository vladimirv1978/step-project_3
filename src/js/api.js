function saveToken(token) {
  sessionStorage.setItem("tokenData", token);
}

async function getTokenData(userEmail, userPassword) {
  let request = await fetch("https://ajax.test-danit.com/api/v2/cards/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ email: userEmail, password: userPassword }),
  });
  let token = await request.text();
  saveToken(token);
}

async function fetchCreateCard(card) {
  try {
    let request = await fetch("https://ajax.test-danit.com/api/v2/cards", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${sessionStorage.getItem("tokenData")}`,
      },
      body: JSON.stringify(card),
    });
    let response = await request.json();
    if (request.ok) {
      response.id ? (card.id = response.id) : console.error("Помилка, Id не отримано");
      return card
    } else {
        throw "вибачте, сервер не відповідає"
    } 
  } catch (error) {
    return error;
  }
}

async function fetchGetAllCards() {
  try {
    let request = await fetch(`https://ajax.test-danit.com/api/v2/cards`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${sessionStorage.getItem("tokenData")}`,
      },
    });
    let response = await request.json();
    if (request.ok) {
      return response;
    } else {
        throw "вибачте, сервер не відповідає"
    } 
  } catch (error) {
    return error;
  }
}

async function fetchGetCard(card) {
  try {
    let request = await fetch(`https://ajax.test-danit.com/api/v2/cards/${card.id}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${sessionStorage.getItem("tokenData")}`,
      },
    });
    let response = await request.json();
    if (request.ok) {
      return response;
    } else {
        throw "вибачте, сервер не відповідає"
    } 
  } catch (error) {
    return error;
  }
}

async function fetchEditCard(card) {
  try {
    let request = await fetch(
      `https://ajax.test-danit.com/api/v2/cards/${card.id}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${sessionStorage.getItem("tokenData")}`,
        },
        body: JSON.stringify(card),
      }
    );
    let response = await request.json();
    if (request.ok) {
      return response;
    } else {
        throw "вибачте, сервер не відповідає"
    } 
  } catch (error) {
    return error;
  }
} 


// .then( response => {
//     if (response instanceof Error) {
//         // ошибка
//     } else {
//         // response - обробка відповіді
//     }
// })


async function fetchDeleteCard(card) {
  console.log(card.id)
  try {
    let request = await fetch(`https://ajax.test-danit.com/api/v2/cards/${card.id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${sessionStorage.getItem("tokenData")}`,
      },
    });
    if (request.status === 200) {
      return console.log(`Видалення картки Id ${card.id} успішне!`);
    } else {
        throw "вибачте, сервер не відповідає"
    } 
  } catch (error) {
    return error;
  }
}

export {
  getTokenData,
  fetchCreateCard,
  fetchGetAllCards,
  fetchGetCard,
  fetchEditCard,
  fetchDeleteCard,
};
