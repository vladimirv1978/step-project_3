import getCards from "./getCards.js";
import { getTokenData } from "./api.js";
import getSectionFilter from "./filter.js";
import openModalWindow from "./openModal.js";

async function userLogin(userEmail, userPassword) {
  await getTokenData(userEmail, userPassword);
  if (sessionStorage.getItem("tokenData")) {
    let header = document.querySelector(".header__logo");
    getSectionFilter(header);
    let logInBtn = document.querySelector(".header__button-login");
    logInBtn.removeEventListener("click", openModalWindow);
    getCards();
  } else {
    document.querySelector(".login__error")?.remove();
    document.querySelector(".login__modal--form").insertAdjacentHTML(
      "beforeend",
      `
    <div class="login__error" style="color:red">Incorrect username or password</div>
    `
    );
  }
}

export default userLogin;
