Завдання:

Розробити середу для створення карток-візитів до лікарів: терапевт, кардіолог, стоматолог.


**Склад групи:**

1. [Вікторія Демидова.](https://gitlab.com/vvictoriademydova)
2. [Батрак Микола.](https://gitlab.com/nikolajbatrak49)
3. [Володимир .](https://gitlab.com/vladimirv1978)

**Завдання для студента №1:**
- _Модальні вікна [Login](https://gitlab.com/vladimirv1978/step-project_3/-/blob/main/src/js/modal.js), [createModalVisit](https://gitlab.com/vladimirv1978/step-project_3/-/blob/main/src/js/chooseDoctor.js), [ModalLogin](https://gitlab.com/vladimirv1978/step-project_3/-/blob/main/src/js/userLogin.js)._
- _Стилізація та структура елементів сайту._

**Завдання для студента №2:**
- _API [AllFunc](https://gitlab.com/vladimirv1978/step-project_3/-/blob/main/src/js/api.js)._
- _[Пошук по меті візиту, сортування за статусом та терміновісттю. Drag & drop карток](https://gitlab.com/vladimirv1978/step-project_3/-/blob/main/src/js/filter.js)._

**Завдання для студента №3:**

- _Клас [Visit](https://gitlab.com/vladimirv1978/step-project_3/-/blob/main/src/js/visit.js) та всі дочірні класи._


Залишкові компоненти створювались спільно.


